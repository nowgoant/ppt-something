title: wxsmall
speaker: Leo
url: https://github.com/nowgoant/pptGulpWebpack
transition: cards
files: /js/index.js,/css/index.css

[slide style="background-image:url('/img/bg1.png')"]
# 微信小程序初体验

[slide style="background-image:url('/img/bg1.png')"]

# 回归历史痛点 {:&.flexbox.vleft}
 - 需要重复安装（每个新项目都需重新安装） {:&.rollIn}
 - 需手动创建项目
 - 需手动复制JRMUI(UI组件库)
 - 不支持项目模板
 - 命令繁琐，结构不清晰
 - 自动打包（zip）,上传预发服务器权限问题

[slide style="background-image:url('/img/bg1.png')"]

# Joyer

- 提供了前端开发基础的UI和业务组件 {:&.rollIn}
- 集成调试，构建，布署，项目模板等一系列开发工具  

[slide style="background-image:url('/img/bg1.png')"]

# 环境配置 {:&.flexbox.vleft}
### Git And Node 4.x.xLTS 注意：勾选自动加环境变量
### [Gulp](http://www.gulpjs.com.cn/) 
```javascript
npm install -g gulp && gulp -v
```
### [WebPack](http://webpack.github.io/)
```javascript
npm install -g webpack && webpack -v
```
### [SASS](http://sass-lang.com/) [安装教程](http://www.w3cplus.com/sassguide/)

### [Compass](http://compass-style.org/) 和 Compass-normalize
```javascript
 gem install compass
gem install compass-normalize
```
### [joyer](https://www.npmjs.com/package/joyer)
```javascript
 npm install -g joyer
joyer [-V|-h]
```

[slide style="background-image:url('/img/bg1.png')"]

# 基本用法 {:&.flexbox.vleft}
```javascript  
  //创建工程
  joyer init myapp

  //开发
  joyer dev 
  
  //编译
  joyer build

  //发布
  joyer deploy 

  //开发文档
  joyer doc 

  //配置信息
  joyer config 
```
 启动本地服务： http://localhost:3000/index.html


[slide style="background-image:url('/img/bg1.png')"]

# 功能介绍 {:&.flexbox.vleft}
 - 支持测试，预发，线上三种开发流程 {:&.rollIn}
 - 项目都有一个单独的配置文件
 - 支持项目模板,HTML模板
 - 支持将sass（compass）编译为css,解决兼容性问题
 - 支持ES6,MD5,小图转Base64
 - 支持编辑器js代码校验[ESLint]
 - 自动压缩优化html、js、css、图片文件、svg(自动生成font-icon) 
 - 内置webpack，browserSync（多端测试），liveReload(实时监听文件,自动更新UI)


[slide style="background-image:url('/img/bg1.png')"]

# 未来方向？ {:&.flexbox.vleft}
 - 支持React,Vue组件化 {:&.rollIn}
 - 支持JavaScript模板引擎
 - 丰富项目模板
 - JRMUI组件库优雅更新和加载 
 - 优化本地服务器切换问题
 - 一键部署预发 
 - ....?
  

[slide style="background-image:url('/img/bg1.png')"]

#Thank You
## Q&A