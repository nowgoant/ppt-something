# PPT 

## 安装
 - `git clone https://github.com/nowgoant/ppt-something.git `
 - `npm install -g nodeppt`
 - `npm install `

## 使用
 - 开发: `npm start`
 - 生成: `npm run generate`
 - 部署到GITHUB: `npm run deploy`
 
## 感谢
 - [nodeppt](https://github.com/ksky521/nodePPT)
